#!/usr/bin/python3
from PyQt5 import QtWidgets, uic, QtCore, QtNetwork
from PyQt5.QtNetwork import QUdpSocket
from PyQt5.QtGui import QTextCursor, QRegExpValidator
from PyQt5.QtCore import QRegExp, QRunnable, pyqtSlot, pyqtSignal, QThreadPool, QObject
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog

import sys, traceback, io
import socket
import json
import csv
from datetime import datetime, date
from os import path


class ThermoReader(QMainWindow):
    datagram_recieved = pyqtSignal(str)

    def __init__(self):
        super(ThermoReader, self).__init__()
        uic.loadUi('data/main.ui', self)
        self.running = True
        self.threadpool = QThreadPool()
        print("Multithreading with maximum %d threads" %
              self.threadpool.maxThreadCount())

        self.client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM,
                                    socket.IPPROTO_UDP)
        self.client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        self.client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.client.bind(("", 44444))

        self.load_widgets()
        self.start_pooling_udp()
        self.show()

    @QtCore.pyqtSlot()
    def on_cleartemp1_clicked(self):
        self.clear_table(self.templist1)

    @QtCore.pyqtSlot()
    def on_savetemp1_clicked(self):
        self.save_to_csv(self.templist1)
        self.clear_table(self.templist1)

    def save_to_csv(self, table):
        fname, _ = QFileDialog.getSaveFileName(self, 'Save Data', '',
                                               "CSV files (*.csv)")
        if not fname:
            return

        csv_appending = False
        if path.exists(fname):
            csv_appending = True

        columns = ['date', 'sensor', 'temp1', 'temp2']
        with open(fname, mode='a+') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=columns)
            if not csv_appending:
                writer.writeheader()
            row_count = table.rowCount()
            line = dict()
            for row in range(row_count):
                line['date'] = table.item(row, 0).text()
                line['sensor'] = table.item(row, 1).text()
                line['temp1'] = table.item(row, 2).text()
                line['temp2'] = table.item(row, 3).text()
                writer.writerow(line)

    def clear_table(self, table):
        table.setRowCount(0)
        table.clearContents()

    @QtCore.pyqtSlot()
    def on_cleartemp2_clicked(self):
        self.clear_table(self.templist2)

    @QtCore.pyqtSlot()
    def on_savetemp2_clicked(self):
        self.save_to_csv(self.templist2)
        self.clear_table(self.templist2)

    def add_to_table(self, data, table):
        rowPosition = table.rowCount()
        table.insertRow(rowPosition)
        table.setItem(
            rowPosition, 0,
            QtWidgets.QTableWidgetItem(
                datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
        table.setItem(rowPosition, 1,
                      QtWidgets.QTableWidgetItem(data['sensor']))
        table.setItem(rowPosition, 2,
                      QtWidgets.QTableWidgetItem(str(data['temp1'])))
        table.setItem(rowPosition, 3,
                      QtWidgets.QTableWidgetItem(str(data['temp2'])))

    def sensor1_udp_read(self, progress_callback):
        while self.running:
            data, addr = self.client.recvfrom(1024)
            json_msg = data.decode('utf-8').replace("'", '"')
            dict_msg = json.loads(json_msg)
            self.add_to_table(dict_msg, self.templist1)
            #progress_callback.emit(data)

    def sensor2_udp_read(self, progress_callback):
        while self.running:
            progress_callback.emit("Running 2")

    def start_pooling_udp(self):
        sensor1_polling = Worker(self.sensor1_udp_read)
        sensor1_polling.signals.progress.connect(self.process_data)
        #sensor2_polling = Worker(self.sensor2_udp_read)
        #sensor2_polling.signals.progress.connect(self.process_data)

        self.threadpool.start(sensor1_polling)
        # self.threadpool.start(sensor2_polling)

    def data_ready(self):
        print("data ready")

    def process_data(self, data):
        print(data)

    def load_widgets(self):
        self.templist1 = self.findChild(QtWidgets.QTableWidget, 'templist1')
        self.templist2 = self.findChild(QtWidgets.QTableWidget, 'templist2')

    def closeEvent(self, event):
        self.running = False
        self.threadpool.cl
        event.accept()


class Worker(QRunnable):
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()
        self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(
                result)  # Return the result of the processing
        finally:
            self.signals.finished.emit()  # Done


class WorkerSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(str)


def main():
    app = QApplication(sys.argv)
    ex = ThermoReader()
    sys.exit(app.exec_())


if __name__ == "__main__":
    print("Starting reader")  #
    main()